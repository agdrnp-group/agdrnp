# Informations generales
## **AGDRNP** : Application de Gestion de la Denomination des Rues et de la Numérotation des Parcelles
 Ce projet est une application web permettant de gérer l'urbanisme et la voirie d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires

 *Pour plus de renseignements, consulter les manuels d'utilisation.*

#
### **Installation sur Linux :**
#### Pre-requis :
   - Installer Node.js si ce n'est pas déjà fait (https://nodejs.org) :
      ``` bash
      $ sudo apt install nodejs
      ```
   - Installer MongoDB si ce n'est pas déjà fait (https://www.mongodb.com/fr) <br/>
      *Suivre le descriptif suivant la distribution Linux utilisée : https://docs.mongodb.com/manual/administration/install-on-linux/*
    <br/><br/>
   - Télécharger les données cadastrales sur le site du gouvernement via le
      formulaire en bas de page, enregistrer ce fichier dans le dossier server sous le nom cadastreParcelle.json : https://cadastre.data.gouv.fr/datasets/cadastre-etalab

#### Installation :
  - Ouvrir un terminal
  - Décompresser l'archive telechargée :
    ``` bash
    $ tar zxfv nom_de_l_archive.tar.gz
    ```
  - Aller dans le dossier `services` :
    ``` bash
    $ cd source/client/src/services
    ```
  - Modifier la mention http://localhost:3000 par l'adresse IP du serveur dans le fichier Api.js :
    ``` bash
    $ vim Api.js
    ```
    puis
    ``` javascript
    export default () => axios.create({
      baseURL: 'http://localhost:3000/',
    });
    ```
  - Modifier ensuite le fichier global de configuration `config.global.js` afin d'intégrer les spécificités de votre collectivité :
    ``` bash
    $ cd ../..
    ```
  - Installer les dépendances :
    ``` bash
    $ npm install
    ```
  - Lancer la production côté client :
    ``` bash
    $ npm run build
    ```
  - Aller dans le dossier source/server puis installer les dépendances :
    ``` bash 
    $ cd ../server
    $ npm install
    ```
  - Lancer le serveur de manière à ce qu’il reste actif après avoir fermé le terminal :
    ``` bash
    $ nohup npm start 0</dev/null &
    ```
  - Ouvrir un nouveau terminal
  - Aller dans source/server :
    ``` bash
    $ cd source/server
    ```
  - Lancer les scripts permettant le remplissage automatique de la base de données :
      ``` bash
      $ sh launchScripts.sh
      ```
    *Ces scripts ne correspondent peut-être pas exactement à vos besoins et devront sûrement être modifiés avant leur utilisation. Pour plus d'informations, voir les commentaires dans les fichiers de type `source/server/inserer*.js`*<br/><br/>
  - Les données cadastrales pourront être mises à jour après le téléchargement des nouvelles données :
      ``` bash
        $ node miseajourParcelle.js
      ```

#
### **Installation sur Windows :**
#### Pre-requis :
   - Installer Node.js si ce n'est pas déjà fait (https://nodejs.org/en/download/)
   - Installer MongoDB si ce n'est pas déjà fait (Suivre le descriptif suivant : https://docs.mongodb.com/manual/tutorial/install-mongodb-on-windows/)
    <br/><br/>
   - Télécharger les données cadastrales sur le site du gouvernement via le
      formulaire en bas de page, enregistrer ce fichier dans le dossier server sous le nom cadastreParcelle.json : https://cadastre.data.gouv.fr/datasets/cadastre-etalab

#### Installation :
  - Ouvrir un terminal
  - Décompresser l'archive telechargée :
    ``` bash
    $ tar zxfv nom_de_l_archive.tar.gz
    ```
  - Aller dans le dossier `services` :
    ``` batch
    cd source/client/src/services
    ```
  - Modifier la mention http://localhost:3000 par l'adresse IP du serveur dans le fichier Api.js :
    ``` javascript
    export default () => axios.create({
      baseURL: 'http://localhost:3000/',
    });
    ```
  - Modifier ensuite le fichier global de configuration `config.global.js` afin d'intégrer les spécificités de votre collectivité :
    ``` batch
    cd ../..
    ```
  - Installer les dépendances :
    ``` batch
    npm install
    ```
  - Lancer la production côté client :
    ``` batch
    npm run build
    ```
  - Aller dans le dossier source/server puis installer les dépendances :
    ``` batch 
    cd ../server
    npm install
    ```
  - Lancer le serveur de manière à ce qu’il reste actif après avoir fermé le terminal :
    ``` batch
    nohup npm start 0</dev/null &
    ```
  - Ouvrir un nouveau terminal
  - Aller dans source/server :
    ``` batch
    cd source/server
    ```
  - Lancer les scripts permettant le remplissage automatique de la base de données :
      ``` batch
      launchScripts.cmd
      ```

      Si le script ne s’arrête pas automatiquement, il suffit de fermer la console et d’en ouvrir une autre. Il faut ensuite retourner dans le dossier `source/server` et lancer les commandes suivantes :
      ``` batch
      node insererLieudit.js
      node insererRue.js
      node insererProprietaire.js
      node insererParcelle.js
      node insererMateriau.js
      node insererUtilRolePerm.js
      ```
    *Ces scripts ne correspondent peut-être pas exactement à vos besoins et devront sûrement être modifiés avant leur utilisation. Pour plus d'informations, voir les commentaires dans les fichiers de type `source/server/inserer*.js`*<br/><br/>
  - Les données cadastrales pourront être mises à jour après le téléchargement des nouvelles données :
      ``` batch
      $ node miseajourParcelle.js
      ```

 *Pour plus de renseignements, consulter le manuel d'installation.*
#
#### Auteurs
 Étudiants à l'IUT de Vannes :
  - Sebastien GAVIGNET
  - Alizee HAMON
  - Alex MAINGUY
  - Alan VADELEAU
  - Aurelien MARCHAND