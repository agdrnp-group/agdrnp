/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/**
 * File to insert the owners
 * 
 * WARNING : This file may not be used like this, without modifications.
 * Its behavior may not be exactly the same as the one you except, so changes may be made.
 */
// Indicate the beginning of the script
console.log('DEBUT INSERTION PROPRIÉTAIRES');

// Import module to read and write files
const fs = require('fs');
// Import module to discuss with the database
const mongoose = require('mongoose');
// Import the needed database model
const Proprietaire = require('./models/proprietaire');
// Import the loading bar to display the script's progress
const LoadingBar = require('./LoadingBar');

// Read the file with the owners
const fileJSON = fs.readFileSync('./proprietaire.json');
// Get the JSON object
const Tpro = JSON.parse(fileJSON).root.dataroot.T_proprietaire;

// Read the file with the postal codes
const fileJSON2 = fs.readFileSync('./codePostal.json');
// Get the JSON object
const TcodePro = JSON.parse(fileJSON2).root.dataroot.T_code_pro;

// Read the file with the streets' names and their codes which link to the owners' street codes
const fileJSON3 = fs.readFileSync('./nomRue.json');
// Get the JSON object
const TnomRue = JSON.parse(fileJSON3).root.dataroot.T_nom_de_la_rue;

// Connection to the MongoDB database
mongoose.connect('mongodb://localhost:27017/Mairie', { poolSize: 10, useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function (callback) {
    console.log('Base de données connectée');
});

// Create a new LoadingBar instance
const loadBar = new LoadingBar(Tpro.length, 'owners');

// Array to store the postal codes
var tabCode = [];
// Array to store the cities
var tabVille = [];
// Civility
let iCivilite;
// Spouse's civility
let iCiviliteC;
// Whole name
let nomPrenom;
// Lastname
let iNom;
// Firstname
let iPrenom;
// Spouse's lastname
let iNomC;
// Spouse's firstname
let iPrenomC;
// Company name
let iNomEntreprise;
// Street number
let iNumero;
// Bis, ter, ...
let iBisTer = '';
// Street
let iRue;
// Address supplement
let iComplement = '';
// Said location
let iLieuDit = '';
// Postal code
let iCodePostal;
// City
let iVille;
// Country
let iPays;
// Owner code
let codePro;

// Boolean to know if the object being added is valid
let isValid = true;
// Land lot's numbers already checked
let numPar = [];
// Land lot's owners already added
let addedPro = [];
// Index to store the number of elements already added
let index = 0;

/**
 * Return the correct civility according to the initial string given
 * @param {The initial civility string to check} civ 
 * @param {Boolean to know if its the owner or its spouse} isPro 
 */
function setCivilite(civ, isPro) {
    if (civ === 'MR' || civ === 'ME' || civ === 'MM') {
        // Return Mr
        return 'Monsieur';
    } else if (civ === 'M' || civ === 'MD') {
        // Return Mrs
        return 'Madame';
    } else if (civ === '0' && isPro) {
        // Return Company
        return 'Entreprise';
    } else {
        return '';
    }
}

// Delete all the owners in the database
Proprietaire.deleteMany({}, function (error) {
    if (error) {
        console.error('ERROR : ' + error);
    } else {
        let idCodeP;
        let codeP;
        let villeCodeP;

        /**
         * Fill with data the cities and postal codes arrays
         */
        TcodePro.forEach((data) => {
            idCodeP = data.code_postal_pro;
            codeP = data.CP_PRO;
            villeCodeP = data.Ville_Propri;

            if (codeP !== undefined) {
                tabCode[idCodeP] = codeP;
            } else {
                tabCode[idCodeP] = '';
            }
            tabVille[idCodeP] = villeCodeP;
        });

        /**
         * Asynchronous function to add the streets in the database
         */
        async function addPro() {
            // Start writing the first loading bar
            loadBar.init();

            // For each owner
            for (pro of Tpro) {
                // If the land lot has not already been added
                if (!numPar.includes(pro.N_parcelle)) {
                    // Reset variables
                    iCivilite = '';
                    iCiviliteC = '';
                    nomPrenom = [];
                    iNom = '';
                    iPrenom = '';
                    iNomC = '';
                    iPrenomC = '';
                    iNomEntreprise = '';
                    iNumero = '';
                    iRue = '';
                    iComplement = '';
                    iBisTer = '';
                    iLieuDit = '';
                    iCodePostal = '';
                    iVille = '';
                    iPays = '';
                    isValid = true;

                    // Civility
                    if (pro.civilite !== undefined) {
                        iCivilite = setCivilite(pro.civilite.toUpperCase(), true);
                    }

                    if (iCivilite !== 'Entreprise') {
                        // Owner's names
                        // Split the full name to get the lastname and firstname
                        if (pro.Nom_proprietaire !== undefined) {
                            nomPrenom = pro.Nom_proprietaire.split(' ');
                            nomPrenom.forEach((name) => {
                                if (name === name.toUpperCase()) {
                                    iNom += name + ' ';
                                } else {
                                    iPrenom += name + ' ';
                                }
                            });
                        } else {
                            isValid = false;
                        }

                        // Civility of the spouse
                        if (pro.civilité_conjoint !== undefined) {
                            iCiviliteC = setCivilite(pro.civilité_conjoint.toUpperCase(), false);
                        }

                        // Spouse's names
                        // Split the full name to get the lastname and firstname
                        if (pro.Nom_conjoint !== undefined) {
                            nomPrenom = pro.Nom_conjoint.split(' ');
                            nomPrenom.forEach((name) => {
                                if (name === name.toUpperCase()) {
                                    iNomC += name + ' ';
                                } else {
                                    iPrenomC += name + ' ';
                                }
                            });
                        }
                    } else {
                        // Company name
                        nom = pro.Nom_proprietaire;

                        if (nom !== null && nom !== undefined)  {
                            iNomEntreprise = nom.trim().toUpperCase();
                        } else {
                            isValid = false;
                        }
                    }

                    // Street number
                    if (pro.Numero_pro !== undefined && pro.Numero_pro !== 0) {
                        iNumero = pro.Numero_pro.trim();
                    }

                    // BisTer
                    if (pro.Bis_ter !== undefined) {
                        iBisTer = pro.Bis_ter.trim().toLowerCase();
                    }

                    // Street name
                    if (pro.Adresse_pro !== undefined) {
                        iRue = pro.Adresse_pro.trim().replace(/(bis |ter )/g, '');
                    } else {
                        if (pro.nom_rue !== undefined) {
                            var rue = TnomRue.find(rue => rue.nom_rue.toUpperCase() === pro.nom_rue.toUpperCase());
                            if (rue !== undefined) {
                                iRue = rue.rue;
                            }
                        }
                    }

                    // Address supplement
                    if (pro.complement !== undefined) {
                        iComplement = pro.complement.trim();
                    }

                    // Said location
                    if (pro.lieu_dit !== undefined) {
                        iLieuDit = pro.lieu_dit.trim();
                    }

                    codePro = pro.code_postal_pro;
                    // Postal code
                    iCodePostal = tabCode[codePro];
                    // City
                    iVille = tabVille[codePro];

                    // Check the owner code to know from which country he/she comes
                    switch(codePro) {
                        case 81:
                            iPays = 'ALLEMAGNE';
                        case 92:
                            iPays = 'BELGIQUE';
                        case 125:
                            iPays = 'ROYAUME-UNI';
                        case 132:
                            iPays = 'SINGAPOUR';
                        case 205:
                            iPays = 'PAYS-BAS';
                        case 263:
                            iPays = 'CANADA';
                        case 311:
                            iPays = 'LUXEMBOURG';
                        case 115470:
                            iPays = 'RUSSIE';
                        default:
                            iPays = 'FRANCE';
                    }

                    if (isValid) {
                        // Create the new owner
                        var new_proprio = new Proprietaire({});
                        new_proprio.civilite = iCivilite;
                        if (iCivilite !== 'Entreprise') {
                            new_proprio.civiliteConjoint = iCiviliteC;
                            new_proprio.nom = iNom.trim();
                            new_proprio.prenom = iPrenom.trim();
                            new_proprio.nomConjoint = iNomC.trim();
                            new_proprio.prenomConjoint = iPrenomC.trim();
                        } else {
                            new_proprio.nomEntreprise = iNomEntreprise.trim();
                        }
                        new_proprio.numero = iNumero;
                        new_proprio.rue = iRue;
                        new_proprio.bisTer = iBisTer;
                        new_proprio.complement = iComplement;
                        new_proprio.lieuDit = iLieuDit;
                        new_proprio.codePostal = iCodePostal;
                        new_proprio.ville = iVille;
                        new_proprio.pays = iPays;

                        // Check if a similar owner has not already been added
                        if (!addedPro.find((prop) => {
                            return prop.civilite === new_proprio.civilite
                                && prop.nomPro === pro.Nom_proprietaire
                                && prop.numero === new_proprio.numero
                                && prop.rue === new_proprio.rue
                                && prop.codePostal === new_proprio.codePostal;
                            })
                        ) {
                            try {
                                // Insert the owner in the database
                                await new_proprio.save();
                                // Add the land lot number added in the array to store them
                                numPar.push(pro.N_parcelle);
                                // Add the owner added in the array to store them
                                addedPro.push({
                                    civilite: new_proprio.civilite,
                                    nomPro: pro.Nom_proprietaire,
                                    numero: new_proprio.numero,
                                    rue: new_proprio.rue,
                                    codePostal: new_proprio.codePostal
                                });
                            } catch (err) {
                                loadBar.displayMessage('ERROR : ' + err);
                            }
                        }
                    } else {
                        loadBar.displayMessage('ERROR : The owner is not valid and has not been added');
                    }
                }
                index++;
                // Update the loading bar
                loadBar.updateBar(index);
            }
        }
        addPro()
            .then(() => {
                // Disconnect from the database
                mongoose.connection.close();
                // Indicate the end of the script
                console.log('FIN INSERTION PROPRIÉTAIRES' + '\n');
            });
    }
});
