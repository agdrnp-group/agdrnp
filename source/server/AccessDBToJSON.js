/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/**
 * File to convert .xml file from an Access database into a json.
 *  
 * To create the .xml file :
 * - Open the Access database
 * - Make a right-click on the table to convert
 * - Select Export -> XML file
 * - Select the location of the export file and click on Ok
 * - Then click on Other options
 * - In the schema menu, select Put the schema in the XML file
 * - Then click on Ok and Close
 */
// Import modules to read and transform files
var convert = require('xml-js');
var fs = require('fs');

// Options for the module to convert xml into json
var options = {
    compact: true,
    spaces: 4,
    trim: true,
    ignoreDeclaration: true,
    ignoreAttributes: true,
    ignoreDoctype: true
};

/**
 * Convert the file containing all the owners and their land lots
 */
// Read the .xml file
var xmlDoc = fs.readFileSync('Tproprietaire.xml', 'utf8');
// Convert the .xml into .json
var json = convert.xml2json(xmlDoc, options);
// Change data from the initial database
// Replace space characters with '_' for example
json = json.replace(/_x0020_/g, '_');
json = json.replace(/_x00B0_/g, '');
json = json.replace(/xsd:schema/g, 'xsdSchema');
// Change owners properties' name
json = json.replace(/Bât_a-_Bât_B/g, 'complement');
json = json.replace(/nom_de_lieux_dits/g, 'lieu_dit');
json = json.replace(/Geolocalisation_parcelle_LAT/g, 'latitudePar');
json = json.replace(/Geolocalisation_parcelle_LON/g, 'longitudePar');
// Replace accents with the good character
json = json.replace(/("[^:,]*)é([^:,]*":)/g, '$1e$2');
// Because some properties contain more than just one 'é'
json = json.replace(/("[^:,]*)é([^:,]*":)/g, '$1e$2');
json = json.replace(/("[^:,]*)à([^:,]*":)/g, '$1a$2');
// Convert the json string into a JSON Object
json = JSON.parse(json);
// Delete some unwanted properties
json = cleanJson(json);
// Delete the .xml schema
delete json.root.xsdSchema;
// Write a new file with the .json content
fs.writeFile('proprietaire.json', JSON.stringify(json, null, 4), 'utf8', () => {});

/**
 * Convert the file containing all the streets
 */
// Read the .xml file
var xmlDoc2 = fs.readFileSync('TnomRue.xml', 'utf8');
// Convert the .xml into .json
var json2 = convert.xml2json(xmlDoc2, options);
// Change data from the initial database
// Replace space characters with '_' for example
json2 = json2.replace(/_x0020_/g, '_');
json2 = json2.replace(/xsd:schema/g, 'xsdSchema');
// Change streets properties' name
json2 = json2.replace(/DEPART_RUE_Géolocalisation_rue_LAT/g, 'latitudeD');
json2 = json2.replace(/DEPART_RUE_Géolocalisation_rue_LON/g, 'longitudeD');
json2 = json2.replace(/FIN_RUE_Géolocalisation_rue_LAT/g, 'latitudeF');
json2 = json2.replace(/FIN_RUE_Géolocalisation_rue_LON/g, 'longitudeF');
// Convert the json string into a JSON Object
json2 = JSON.parse(json2);
// Delete some unwanted properties
json2 = cleanJson(json2);
// Delete the .xml schema
delete json2.root.xsdSchema;
// Write a new file with the .json content
fs.writeFile('nomRue.json', JSON.stringify(json2, null, 4), 'utf8', () => {});

/**
 * Convert the file containing all the links between streets and streets where the owners live
 */
// Read the .xml file
var xmlDoc3 = fs.readFileSync('TcodePro.xml', 'utf8');
// Convert the .xml into .json
var json3 = convert.xml2json(xmlDoc3, options);
// Change data from the initial database
// Replace space characters with '_' for example
json3 = json3.replace(/_x0020_/g, '_');
json3 = json3.replace(/xsd:schema/g, 'xsdSchema');
// Convert the json string into a JSON Object
json3 = JSON.parse(json3);
// Delete some unwanted properties
json3 = cleanJson(json3);
// Delete the .xml schema
delete json3.root.xsdSchema;
// Write a new file with the .json content
fs.writeFile('codePostal.json', JSON.stringify(json3, null, 4), 'utf8', () => {});

/**
 * Used to delete unwanted propreties such as 'a: { _text: value }' to 'a: value'
 * @param {JSON Object} json
 * @from https://github.com/owncloud/owncloud-sdk/pull/19/files#diff-b9b823e719730e39048dffbb512e743cR785
 */
function cleanJson(json) {
    for (var key in json) {
        var a = recursiveCleanse(json[key]);
        json[key] = a;
    }
    return json;
}

/**
 * Recursive loop to delete unwanted properties such as 'a: { _text: value }' to 'a: value'
 * @param {JSON Object} json
 * @from https://github.com/owncloud/owncloud-sdk/pull/19/files#diff-b9b823e719730e39048dffbb512e743cR785
 */
function recursiveCleanse(json) {
    if (typeof(json) !== 'object') {
        return json;
    }

    for (var key in json) {
        if (key === '_text') {
            return json[key];
        }
        json[key] = recursiveCleanse(json[key]);
    }
    return json;
}