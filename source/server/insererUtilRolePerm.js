/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/**
 * File to insert the users and their account's type and permissions
 */
// Indicate the beginning of the script
console.log('DEBUT INSERTION PERMISSIONS UTILISATEURS');

// Import the needed database model
const Permission = require('./models/permission');
const Role = require('./models/role');
const Utilisateur = require('./models/utilisateur');
// Import module to discuss with the database
const mongoose = require('mongoose');
// Import module to check the encrypted password
const bcrypt = require('bcryptjs');

// Connection to the MongoDB database
mongoose.connect('mongodb://localhost:27017/Mairie', { poolSize: 10, useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function (callback) {
    console.log('Base de données connectée');
});

// Delete all the permissions in the database
Permission.deleteMany({}, async function (error) {
    if (error) {
        console.error(error);
    }

    // Create a permission for the users' management
    var new_permission = new Permission({});
    new_permission.nomPermission = 'gestion des utilisateurs';
    await new_permission.save(function (error) {
        if (error) {
            console.error(error);
        }
    });

    // Create a permission for the streets', owners' and land lots' management
    new_permission = new Permission({});
    new_permission.nomPermission = 'gestion des rues/par/pro';
    await new_permission.save(function (error) {
        if (error) {
            console.error(error);
        }
    });

    // Create a permission for the streets', owners' and land lots' visualization
    new_permission = new Permission({});
    new_permission.nomPermission = 'consultation des rues/par/pro';
    await new_permission.save(function (error) {
        if (error) {
            console.error(error);
        }
    });

    // Create a permission for the letters' management
    new_permission = new Permission({});
    new_permission.nomPermission = 'gestion des courriers';
    await new_permission.save(function (error) {
        if (error) {
            console.error(error);
        }
    });

    // Delete all the account's types in the database
    Role.deleteMany({}, async function (error) {
        if (error) {
            console.error(error);
        }

        // Search the permission in the database
        var idGestionUtil = await Permission.findOne({ nomPermission: 'gestion des utilisateurs'});
        var idRuesParPro = await Permission.findOne({ nomPermission: 'gestion des rues/par/pro' });
        var idconsultRuesParPro = await Permission.findOne({ nomPermission: 'consultation des rues/par/pro' });
        var idGestionCourriers = await Permission.findOne({ nomPermission: 'gestion des courriers' });

        // Create account's type and give them permissions
        // Administrator account's type
        var new_role = new Role({});
        new_role.nomRole = 'Administrateur';
        var listePermission = [idGestionUtil._id, idRuesParPro._id, idconsultRuesParPro._id];
        new_role.listePermission = listePermission;
        await new_role.save(function (error) {
            if (error) {
                console.error(error);
            }
        });

        // Technical manager account's type
        new_role = new Role({});
        new_role.nomRole = 'Responsable technique';
        listePermission = [idRuesParPro._id, idconsultRuesParPro._id];
        new_role.listePermission = listePermission;
        await new_role.save(function (error) {
            if (error) {
                console.error(error);
            }
        });

        // Consultant account's type
        new_role = new Role({});
        new_role.nomRole = 'Consultant';
        listePermission = [idconsultRuesParPro._id, idGestionCourriers];
        new_role.listePermission = listePermission;
        await new_role.save(function (error) {
            if (error) {
                console.error(error);
            }
        });

        // Delete all the users in the database
        Utilisateur.deleteMany({}, async function (error) {
            if (error) {
                console.error(error);
            }

            // Search the Administrator account's type
            var roleAdmin = await Role.findOne({ nomRole: 'Administrateur' });

            // Create a new user : admin
            var new_utilisateur = new Utilisateur({});
            new_utilisateur.identifiant = 'admin';
            // Hash its password
            new_utilisateur.motDePasse = bcrypt.hashSync('admin', 8);
            new_utilisateur.nom = 'admin';
            new_utilisateur.prenom = 'admin';
            new_utilisateur.role = roleAdmin._id;
            // Insert it in the database
            await new_utilisateur.save(function (error) {
                if (error) {
                    console.error(error);
                }
                // Disconnect from the database
                mongoose.disconnect();
                // Indicate the end of the script
                console.log('FIN INSERTION PERMISSIONS UTILISATEURS');
            });
        });
    });
});