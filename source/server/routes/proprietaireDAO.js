/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/**
 * Manage owner data
 */
// Import schemas and router
const express = require('express');
const router = express.Router();
const Proprietaire = require('../models/proprietaire');
const verifConnexion = require("../verifConnexion");
/* eslint no-underscore-dangle: 0 */
// const deepEqual = require('deep-equal');

/**
 * Add an owner in the database
 */
router.post('/ajouter', verifConnexion.verif, (req, res) => {
    // Get variables sent from the client-side
    var pCivilite = req.body.civilite;
    if (pCivilite !== 'Entreprise') {
      var pCiviliteConjoint = req.body.civiliteConjoint;
      var pNom = req.body.nom;
      var pPrenom = req.body.prenom;
      var pNomConjoint = req.body.nomConjoint;
      var pPrenomConjoint = req.body.prenomConjoint;
    } else {
      var pNomEntreprise = req.body.nomEntreprise;
    }
    var pNumero = req.body.numero;
    var pBisTer = req.body.bisTer;
    var pRue = req.body.rue;
    var pComplement = req.body.complement;
    var pLieuDit = req.body.lieuDit;
    var pCodePostal = req.body.codePostal;
    var pVille = req.body.ville;
    var pPays = req.body.pays;

    // Create the new owner
    var new_proprietaire = new Proprietaire({});
    new_proprietaire.civilite = pCivilite;
    if (pCivilite !== 'Entreprise') {
      new_proprietaire.civiliteConjoint = pCiviliteConjoint;
      new_proprietaire.nom = pNom;
      new_proprietaire.prenom = pPrenom;
      new_proprietaire.nomConjoint = pNomConjoint;
      new_proprietaire.prenomConjoint = pPrenomConjoint;
    } else {
      new_proprietaire.nomEntreprise = pNomEntreprise;
    }
    new_proprietaire.numero = pNumero;
    new_proprietaire.bisTer = pBisTer;
    new_proprietaire.rue = pRue;
    new_proprietaire.complement = pComplement;
    new_proprietaire.lieuDit = pLieuDit;
    new_proprietaire.codePostal = pCodePostal;
    new_proprietaire.ville = pVille;
    new_proprietaire.pays = pPays;

    // Insert the new owner in the database
    new_proprietaire.save(function (error) {
        if (error) {
            console.log("ERROR : "+error);
            res.send({
              success: false
            });
        } else {
          res.send({
              success: true
          });
        }
    });
});

/**
 * Search an owner by its name
 */
router.post('/rechercher', verifConnexion.verif, (req, res) => {
  // Get the name from the client-side in an array of words
  var words = req.body.words;
  // Get the name from the client-side in a string
  var searchText = req.body.searchText;
  var id = req.body.id;
  // Search in the database by its id if it is provided
  if (id !== undefined && id !== null) {
    Proprietaire.find({ _id: id }, function (error, proprietaire) {
      if (error) { console.error(error); }
      res.send(proprietaire);
    });
  } else if (words !== undefined && words !== null) {

    if (words.length > 0) {
      /**
       * Asynchronous and recursive function to search all the owners corresponding to the words given
       * @param {Array of words} words
       * @param {Index to look for} i
       * @return {The owners matching all the words}
       */
      async function findData(words, i) {
        var results = await Proprietaire.find({
          $or: [
            {
              nom: {
                $regex: words[i],
                $options: "i"
              }
            },
            {
              prenom: {
                $regex: words[i],
                $options: "i"
              }
            },
            {
              nomEntreprise: {
                $regex: words[i],
                $options: "i"
              }
            }
          ]
        });
        i++;
        if (i < words.length) {
          // Search the owners who have the next word in their name
          const resultat = await findData(words, i);
          return results.filter(function (n) {
            for (var i=0; i < resultat.length; i++) {
              if (n.id === resultat[i].id) {
                return true;
              }
            }
            return false;
          });
        } else {
          return results;
        }
      }
      // Call the recursive function for the first word
      findData(words, 0).then((resultat) => {
        res.send(resultat);
      });
    } else {
      res.send([]);
    }
  } else if (searchText !== undefined && searchText !== null) {
    // Search with the owner's whole name
    Proprietaire.find({ $text: { $search: searchText } }, function (error, proprietaire) {
      if (error) { console.error(error); }
      res.send(proprietaire);
    });
  } else {
    res.send([]);
  }
});

/**
 * Search all the owners in the database
 */
router.post('/rechercherTous', verifConnexion.verif, (req, res) => {
  // Search in the database
  Proprietaire.find({}, function (error, proprietaire) {
    if (error) { console.error(error); }
    res.send(proprietaire);
  });
});

/**
 * Get a specific owner in the database
 */
router.post('/modifier/:id', verifConnexion.verif, (req, res) => {
  Proprietaire.findById(req.params.id, function (error, proprietaire) {
      if (error) { console.error(error); }
      res.send(proprietaire);
  });
});

/**
 * Update a specific owner in the database
 */
router.put('/modifier/:id', verifConnexion.verif, (req, res) => {
  // Search in the database
  Proprietaire.findById(req.params.id, function (error, proprietaire) {
      if (error) { console.error(error); }

      // Change data
      proprietaire.civilite = req.body.civilite;
      if (req.body.civilite !== 'Entreprise') {
        proprietaire.civiliteConjoint = req.body.civiliteConjoint;
        proprietaire.nom = req.body.nom;
        proprietaire.prenom = req.body.prenom;
        proprietaire.nomConjoint = req.body.nomConjoint;
        proprietaire.prenomConjoint = req.body.prenomConjoint;
      } else {
        proprietaire.nomEntreprise = req.body.nomEntreprise;
      }
      proprietaire.numero = req.body.numero;
      proprietaire.bisTer = req.body.bisTer;
      proprietaire.rue = req.body.rue;
      proprietaire.complement = req.body.complement;
      proprietaire.lieuDit = req.body.lieuDit;
      proprietaire.codePostal = req.body.codePostal;
      proprietaire.ville = req.body.ville;
      proprietaire.pays = req.body.pays;

      // Save changes
      proprietaire.save(function (error) {
          if (error) {
              console.log(error);
          }
          res.send({
              success: true
          });
      });
  });
});

/**
 * Delete a specific owner in the database
 */
router.post('/supprimer/:id', verifConnexion.verif, (req, res) => {
  // Delete the owner
  Proprietaire.deleteOne({
      _id: req.params.id
  }, function (err, proprietaire) {
      if (err) res.send(err);
      res.send({
          success: true
      });
  });
});

// Export the router
module.exports = router;