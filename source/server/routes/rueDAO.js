/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/**
 * Manage street data
 */
// Import schemas and router
const express = require('express');
const router = express.Router();
const Rue = require('../models/rue');
const lieuDit = require('../models/lieudit');
const verifConnexion = require("../verifConnexion");

/**
 * Add a street in the database
 */
router.post('/ajouter', verifConnexion.verif, async (req, res) => {
    // Get variables sent from the client-side
    var pNom = req.body.nom;
    var pLongitudeD = req.body.longitudeD;
    var pLatitudeD = req.body.latitudeD;
    var pCoordInt = req.body.coordInt;
    var pLongitudeF = req.body.longitudeF;
    var pLatitudeF = req.body.latitudeF;
    // Get the said location
    var pLieuDit = await lieuDit.findOne({ nom: req.body.lieuDit });

    // Create the new street
    var new_rue = new Rue({});
    new_rue.nom = pNom;
    new_rue.longitudeD = pLongitudeD;
    new_rue.latitudeD = pLatitudeD;
    new_rue.coordInt = pCoordInt;
    new_rue.longitudeF = pLongitudeF;
    new_rue.latitudeF = pLatitudeF;
    if (pLieuDit !== null && pLieuDit._id !== undefined) {
        new_rue.lieuDit = pLieuDit._id;
    }

    // Insert the new street in the database
    new_rue.save(function (error) {
        if (error) {
            console.log("ERROR : "+error);
            res.send({
              success: false
            });
        } else {
          res.send({
              success: true
          });
        }
    });
});

/**
 * Search a street by its name
 */
router.post('/rechercher', verifConnexion.verif, (req, res) => {
    // Get the name from the client-side
    var nom = req.body.nom;
    if (nom !== null && nom !== undefined && nom !== '') {
        // Search in the database
        Rue.find({ nom: { $regex: nom, $options: "i" }}).populate('lieuDit').exec(function (error, recherche) {
            if (error) { console.error(error); }
            res.send(recherche);
        });
    } else {
        // If no data is provided, get all the streets
        Rue.find({}).populate('lieuDit').exec(function (error, recherche) {
            if (error) { console.error(error); }
            res.send(recherche);
        });
    }
});

/**
 * Search all the streets attached to a specific said location
 */
router.post('/rechercherLieu', verifConnexion.verif, (req, res) => {
    // Get the said location's name from the client-side
    var lieuDit = req.body.lieuDit;
    if (lieuDit !== null && lieuDit !== undefined) {
        // Search in the database
        Rue.find({}).populate({ path: 'lieuDit', match: { nom: { $regex: lieuDit, $options: "i" } } }).exec(function (error, recherche) {
            if (error) { console.error(error); }
            const r = [];
            // Check each element in the research's result
            recherche.forEach((element,i) => {
                if (element.lieuDit !== null) {
                    r.push(element);
                }
                if (i === recherche.length - 1) {
                    res.send(r);
                }
            });
        });
    } else {
        // If no data is provided, get all the streets
        Rue.find({}).populate('lieuDit').exec(function (error, recherche) {
            if (error) { console.error(error); }
            res.send(recherche);
        });
    }
});

/**
 * Get a specific street in the database
 */
router.post('/modifier/:id', verifConnexion.verif, (req, res) => {
    Rue.findById(req.params.id).populate('lieuDit').exec( function (error, rue) {
      if (error) console.error(error);
      res.send(rue);
  });
});

/**
 * Update a specific street in the database
 */
router.put('/modifier/:id', verifConnexion.verif, (req, res) => {
    // Search in the database
    Rue.findById(req.params.id, async function (error, rue) {
        if (error) { console.error(error); }

        // Change data
        rue.nom = req.body.nom;
        rue.longitudeD = req.body.longitudeD;
        rue.latitudeD = req.body.latitudeD;
        rue.coordInt = req.body.coordInt;
        rue.longitudeF = req.body.longitudeF;
        rue.latitudeF = req.body.latitudeF;
        // Get the said location
        var pLieuDit = await lieuDit.findOne({ nom: req.body.lieuDit });
        if (pLieuDit !== null && pLieuDit._id !== undefined) {
            rue.lieuDit = pLieuDit._id;
        }

        // Save changes
        rue.save(function (error) {
            if (error) {
                console.log(error);
            }
            res.send({
                success: true
            });
        });
    });
});

/**
 * Delete a specific street in the database
 */
router.post('/supprimer/:id', verifConnexion.verif, (req, res) => {
    // Delete the street
    Rue.deleteOne({
        _id: req.params.id
    }, function (err) {
        if (err) res.send(err);
        res.send({
            success: true
        });
    });
});

// Export the router
module.exports = router;
