/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/**
 * Manage material's category data
 */
// Import modules to write in the console
const process = require('process');
const rdl = require('readline');

let rl = rdl.createInterface({
    input: process.stdin,
    output: process.stdout,
});

/**
 * To reenable the cursor if the program is shutted down
 */
// Windows workaround
if (process.platform === 'win32') {
    rl.on('SIGINT', function () {
        process.stdout.write('\x1B[?25h');
        process.emit('SIGINT');
    });
}

process.on('SIGINT', function () {
    // graceful shutdown
    process.stdout.write('\x1B[?25h');
    process.exit();
});

/**
 * Create a LoadingBar instance from which it is possible to display
 * the evolution of a data insetion for example.
 * 
 * Inspired from https://blog.bitsrc.io/build-a-command-line-progress-bar-in-node-js-5702a3525a49
 */
class LoadingBar {
    /**
     * Constructor of a LoadingBar
     * @param {Maximum number of elements to insert} nbElementsMax 
     * @param {Category of objects inserted} category 
     */
    constructor(nbElementsMax, category) {
        this.size = 30;
        this.nbElementsMax = nbElementsMax;
        this.category = category;
        this.barString = '';
    }

    /**
     * Initiate the loading bar
     */
    init() {
        // Take the control of the console by disabling the cursor
        process.stdout.write('\x1B[?25l');
        // Display the initial bar
        this.displayBar(0);
    }

    /**
     * Display the loading bar on the console with a message
     * @param {The percentage of the bar to display} percent 
     */
    displayBar(percent) {
        // Clear the line
        process.stdout.clearLine();
        // Set the cursor at the right position
        process.stdout.cursorTo(0);
        // Write the start of the bar
        process.stdout.write('[');
        var actualProgress = parseInt(percent * 30 / 100);
        // Write the characters to show the completed data
        for (let i = 0; i < actualProgress; i++) {
            process.stdout.write('\u2588');
        }
        var progress = this.size-actualProgress;
        // Write the characters to show the remaining data
        for (let i = 0; i < progress; i++) {
            process.stdout.write('.');
        }
        // Write the end of the bar
        process.stdout.write(']\t');
        // Display a message with the percentage of completion
        process.stdout.write(percent + '% of ' + this.nbElementsMax + ' ' + this.category + ' added');
        process.stdout.write(this.barString);
    }

    /**
     * Display a message on the console by disabling the loading bar
     * @param {The message to display on the console} message 
     */
    displayMessage(message) {
        // Enable the cursor
        process.stdout.write('\x1B[?25h');
        // Clear the line
        process.stdout.clearLine();
        // Set the cursor at the right position
        process.stdout.cursorTo(0);
        // Write the message
        console.log(message);
        // Disable the cursor
        process.stdout.write('\x1B[?25l');
        // Display the bar again
        this.displayBar(this.oldPercent);
    }

    /**
     * Update the bar according to the remaining data to insert
     * @param {The actual number of elements inserted} actualNbElements 
     */
    updateBar(actualNbElements) {
        // Determine the actual percentage
        var actualPercent = parseInt(actualNbElements * 100 / this.nbElementsMax);
        if (actualNbElements <= this.nbElementsMax && (this.oldPercent !== actualPercent)) {
            // Set the old percentage
            this.oldPercent = actualPercent;
            // Display the bar
            this.displayBar(this.oldPercent);
            // Write the bar if the insertion is completed
            if (actualNbElements === this.nbElementsMax) {
                // Clear the line
                process.stdout.clearLine();
                // Set the cursor at the right position
                process.stdout.cursorTo(0);
                // Enable the cursor
                process.stdout.write('\x1B[?25h');
                rl.close();
                // Write the bar with a string
                var str = '';
                str += '[';
                for (let i = 0; i < this.size; i++) {
                    str += '\u2588';
                }
                str += ']\t';
                str += this.oldPercent + '% of ' + this.nbElementsMax + ' ' + this.category + ' added';
                // Write the last message
                console.log(str);
            }
        }
    }
}

// Export the loading bar
module.exports = LoadingBar;