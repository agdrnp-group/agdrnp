REM Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)
REM 
REM Contributeurs :
REM - Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
REM - Alizée HAMON (alize2301@gmail.com)
REM - Alex MAINGUY
REM - Aurélien MARCHAND
REM - Alan VADELEAU
REM 
REM Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
REM d'une collectivité à travers plusieurs outils :
REM    - Gestion des rues
REM    - Gestion des propriétaires
REM    - Gestion des parcelles
REM    - Gestion des lieux-dits
REM    - Commande de panneaux ou autres pour la signalisation
REM    - Gestion des propositions de noms de rues
REM    - Gestion de courriers pour du publipostage à destination des propriétaires. 
REM 
REM Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
REM respectant les principes de diffusion des logiciels libres. Vous pouvez
REM utiliser, modifier et/ou redistribuer ce programme sous les conditions
REM de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
REM sur le site "http://www.cecill.info".
REM 
REM En contrepartie de l'accessibilité au code source et des droits de copie,
REM de modification et de redistribution accordés par cette licence, il n'est
REM offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
REM seule une responsabilité restreinte pèse sur l'auteur du programme, le
REM titulaire des droits patrimoniaux et les concédants successifs.
REM 
REM A cet égard l'attention de l'utilisateur est attirée sur les risques
REM associés au chargement, à l'utilisation, à la modification et/ou au
REM développement et à la reproduction du logiciel par l'utilisateur étant 
REM donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
REM manipuler et qui le réserve donc à des développeurs et des professionnels
REM avertis possédant des connaissances informatiques approfondies. Les
REM utilisateurs sont donc invités à charger et tester l'adéquation du
REM logiciel à leurs besoins dans des conditions permettant d'assurer la
REM sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
REM à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 
REM 
REM Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
REM pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
REM termes.
REM 
REM -------------------------------------------------------------------------
REM 
REM Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)
REM 
REM Contributors :
REM - Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
REM - Alizée HAMON (alize2301@gmail.com)
REM - Alex MAINGUY
REM - Aurélien MARCHAND
REM - Alan VADELEAU
REM 
REM This software is a computer program whose purpose is to manage the urban
REM planning and the roads of a community through several tools :
REM    - Street management
REM    - Owner management
REM    - Land lot management
REM    - Said location management
REM    - Orders of road signs and others
REM    - Management of street name suggestions
REM    - Mail management for direct mail to owners.
REM 
REM This software is governed by the CeCILL-B license under French law and
REM abiding by the rules of distribution of free software. You can use, 
REM modify and/ or redistribute the software under the terms of the CeCILL-B
REM license as circulated by CEA, CNRS and INRIA at the following URL
REM "http://www.cecill.info". 
REM 
REM As a counterpart to the access to the source code and rights to copy,
REM modify and redistribute granted by the license, users are provided only
REM with a limited warranty and the software's author, the holder of the
REM economic rights, and the successive licensors have only limited
REM liability. 
REM 
REM In this respect, the user's attention is drawn to the risks associated
REM with loading, using, modifying and/or developing or reproducing the
REM software by the user in light of its specific status of free software,
REM that may mean that it is complicated to manipulate, and that also
REM therefore means that it is reserved for developers and experienced
REM professionals having in-depth computer knowledge. Users are therefore
REM encouraged to load and test the software's suitability as regards their
REM requirements in conditions enabling the security of their systems and/or 
REM data to be ensured and, more generally, to use and operate it in the 
REM same conditions as regards security. 
REM 
REM The fact that you are presently reading this means that you have had
REM knowledge of the CeCILL-B license and that you accept its terms.
REM -----------------------------------------------------------------------
:: Script to launch all the scripts to insert data from an initial AccessDB database
node insererLieudit.js
node insererRue.js
node insererProprietaire.js
node insererParcelle.js
node insererMateriau.js
node insererUtilRolePerm.js
