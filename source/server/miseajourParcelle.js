/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/**
 * File to update the land lots' cadatral data
 * 
 * WARNING : The cadastral data file has to be downloaded manually
 * from : https://cadastre.data.gouv.fr/datasets/cadastre-etalab
 */
// Indicate the beginning of the script
console.log('DEBUT MISE A JOUR PARCELLES');

// Import module to read and write files
const fs = require('fs');
// Import module to discuss with the database
const mongoose = require('mongoose');
// Import the needed database model
const Parcelle = require('./models/parcelle');
// Import the loading bar to display the script's progress
const LoadingBar = require('./LoadingBar');

// Read the file with the land lots' cadastral data
const fileJSON = fs.readFileSync('./cadastreParcelles.json');
// Get the JSON object
const cadastre = JSON.parse(fileJSON).features;

// Connection to the MongoDB database
mongoose.connect('mongodb://localhost:27017/Mairie', { poolSize: 10, useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function (callback) {
    console.log('\n Base de données connectée');
});

// Create a new LoadingBar instance
const loadBar = new LoadingBar(cadastre.length, 'land lots');

// Land lot's number
var numeroPar = '';
// Land lot's cadastral data
var cadPar = [];

// Boolean to know if the object being added is valid
let isValid = true;
// Index to store the number of elements already added
let index = 0;

/**
 * Asynchronous function to update the land lots' cadastral data
 */
async function updatePar() {
    // Start writing the first loading bar
    loadBar.init();
    // Find all the land lots in the database
    const parcelles = await Parcelle.find();
    // For each land lot, try to update its cadastral data
    for (par of parcelles) {
        // Reset variables
        cadPar = [];
        numeroPar = par.numero
        let i = 0;
        let found = false;

        // Check if the land lot has a cadastre
        while (i < cadastre.length && !found) {
            // Search the cadastre associated to the land lot
            let cad = cadastre[i].properties.section.concat(cadastre[i].properties.numero);
            if (cad === numeroPar) {
                isValid = true;
                // Add the cadastral data into the array
                cadastre[i].geometry.coordinates.forEach(element => {
                    cadPar.push(element);
                });
            } else if (numeroPar.includes('/')) {
                // Else, check for each part of the land lot's number if it exists
                nPar = numeroPar.split('/');
                nPar.forEach((element) => {
                    if (cad === element) {
                        isValid = true;
                        // Add the cadastral data into the array
                        cadastre[i].geometry.coordinates.forEach(element => {
                            cadPar.push(element);
                        });
                    }
                });
            }
            i++;
        }
        if (isValid) {
            // Update the land lot's cadastral data
            par.cadastre = cadPar;
            par.save(function (error) {
                if (error) {
                   console.log(error)
                }
            });
        } else {
            loadBar.displayMessage('ERROR : The land load has not been found');
        }
        index++;
        // Update the loading bar
        loadBar.updateBar(index);
    }
}

updatePar()
    .then(() => {
        // Indicate the end of the script
        console.log('\n' + 'FIN MISE A JOUR PARCELLES' + '\n');
    });