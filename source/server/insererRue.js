/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/**
 * File to insert the streets
 * 
 * WARNING : This file may not be used like this, without modifications.
 * Its behavior may not be exactly the same as the one you except, so changes will maybe have to be made.
 */
// Indicate the beginning of the script
console.log('DEBUT INSERTION RUES');

// Import module to read and write files
const fs = require('fs');
// Import module to discuss with the database
const mongoose = require('mongoose');
// Import the needed database model
const Rue = require('./models/rue');
const Lieudit = require('./models/lieudit');
// Import the loading bar to display the script's progress
const LoadingBar = require('./LoadingBar');
// Import the global data from the configuration file
const configGlobal = require('../client/config.global');

// Read the file with the streets' names and their codes which link to the owners' street codes
const fileJSON = fs.readFileSync('./nomRue.json');
// Get the JSON object
const TnomRue = JSON.parse(fileJSON).root.dataroot.T_nom_de_la_rue;

// Read the file with the owners
const fileJSON2 = fs.readFileSync('./proprietaire.json');
// Get the JSON object
const Tpro = JSON.parse(fileJSON2).root.dataroot.T_proprietaire;

// Connection to the MongoDB database
mongoose.connect('mongodb://localhost:27017/Mairie', { poolSize: 10, useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function (callback) {
    console.log('Base de données connectée');
});

// Create a new LoadingBar instance
const loadBar = new LoadingBar(TnomRue.length, 'streets');
// Street's name
var nomRue;
// Start point's latitude
var latitudeD;
// Start point's longitude
var longitudeD;
// End point's latitude
var latitudeF;
// End point's longitude
var longitudeF;
// Said location
var lieuDit;
// Array to store all the said locations
var allLieuxDit = [];

// Boolean to know if the object being added is valid
let isValid = true;
// Array to store the streets that have already been added
let addedRue = [];
// Index to store the number of elements already added
let index = 0;

/**
 * Check if a value is correct
 * @param {The value to check} value 
 * @return {Boolean to know if the value is correct}
 */
function isCorrect(value) {
    return value !== null && value !== undefined && value.trim() !== '';
}

/**
 * Know if the geographical coordinate has to be corrected
 * @param {The value to correct} value 
 * @param {Boolean to indicate if the latitude has to be changed or the longitude} lat 
 * @return {Boolean to know if the coordinate has to be corrected}
 */
function correctCoordinates(value, lat) {
    const regLat = new RegExp(`/^${configGlobal.latitudePrefix}\.\d{6}$/g`);
    const regLon = new RegExp(`/^${configGlobal.longitudePrefix}\.\d{6}$/g`);
    if (lat) {
        return regLat.test(value.replace(/°/g, ''));
    } else {
        return regLon.test(value.replace(/°/g, ''));
    }
}

// Delete all the streets in the database
Rue.deleteMany({}, function (error) {
    if (error) {
        console.error('ERROR : ' + error);
    } else {
        /**
         * Asynchronous function to add the streets in the database
         */
        async function addRue() {
            // Start writing the first loading bar
            loadBar.init();

            // Look in all the owners to get all the said locations
            for (pro of Tpro) {
                if (isCorrect(pro.lieu_dit) && isCorrect(pro.nom_rue) && allLieuxDit.find(lieu => lieu.rue.trim().toUpperCase() === pro.nom_rue.trim().toUpperCase()) === undefined) {
                    allLieuxDit.push({
                        rue: pro.nom_rue.trim(),
                        nom: pro.lieu_dit.trim()
                    });
                }
            }

            // For each street
            for (rue of TnomRue) {
                // Reset variables
                nomRue = '';
                lieuDit = '';
                latitudeD = '';
                longitudeD = '';
                latitudeF = '';
                longitudeF = '';
                isValid = true;
                
                if (isCorrect(rue.nom_rue) && isCorrect(rue.rue)) {
                    nomRue = rue.rue.trim();
                    // Correct geographical coordinates
                    if (isCorrect(rue.latitudeD) && isCorrect(rue.longitudeD) && isCorrect(rue.latitudeF) && isCorrect(rue.longitudeF)) {
                        if (correctCoordinates(rue.latitudeD, true)) {
                            latitudeD = rue.latitudeD.replace(/°/g, '');
                        }
                        if (correctCoordinates(rue.longitudeD, false)) {
                            longitudeD = rue.longitudeD.replace(/°/g, '');
                        }
                        if (correctCoordinates(rue.latitudeF, true)) {
                            latitudeF = rue.latitudeF.replace(/°/g, '');
                        }
                        if (correctCoordinates(rue.longitudeF, false)) {
                            longitudeF = rue.longitudeF.replace(/°/g, '');
                        }
                    }

                    // Assign the said location
                    const tempLieu = allLieuxDit.find(lieu => lieu.rue === rue.nom_rue.trim().toUpperCase());
                    if (tempLieu !== undefined) {
                        // Search in the database the desired said location to get its id
                        const response = await Lieudit.findOne({ nom: tempLieu.nom });
                        if (response !== null && response !== undefined) {
                            lieuDit = response._id;
                        }
                    }
                } else {
                    isValid = false;
                }

                if (addedRue.find((nomR) => nomR === nomRue.toUpperCase()) === undefined) {
                    if (isValid) {
                        // Create the new street
                        var new_rue = new Rue({});
                        new_rue.nom = nomRue;
                        if (lieuDit !== '') {
                            new_rue.lieuDit = lieuDit;
                        }
                        if (latitudeD !== '') {
                            new_rue.latitudeD = latitudeD;
                        }
                        if (longitudeD !== '') {
                            new_rue.longitudeD = longitudeD;
                        }
                        if (latitudeF !== '') {
                            new_rue.latitudeF = latitudeF;
                        }
                        if (longitudeF !== '') {
                            new_rue.longitudeF = longitudeF;
                        }

                        try {
                            // Add the new street in the database
                            await new_rue.save();
                            // Add the street added in the array to store them
                            addedRue.push(new_rue.nom.toUpperCase());
                        } catch (err) {
                            loadBar.displayMessage('ERROR : ' + err);
                        }
                    } else {
                        loadBar.displayMessage('ERROR : The street is not valid and has not been added');
                    }
                }
                index++;
                // Update the loading bar
                loadBar.updateBar(index);
            }
        }
        addRue()
            .then(() => {
                // Disconnect from the database
                mongoose.connection.close();
                // Indicate the end of the script
                console.log('FIN INSERTION RUES' + '\n');
            });
        }
    });
