/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/**
 * File to insert the land lots
 * 
 * WARNING : This file may not be used like this, without modifications.
 * Its behavior may not be exactly the same as the one you except, so changes will maybe have to be made.
 */
// Indicate the beginning of the script
console.log('DEBUT INSERTION PARCELLES');

// Import module to read and write files
const fs = require('fs');
// Import module to discuss with the database
const mongoose = require('mongoose');
// Import the needed database model
const Parcelle = require('./models/parcelle');
const Rue = require('./models/rue');
const Proprietaire = require('./models/proprietaire');
// Import the loading bar to display the script's progress
const LoadingBar = require('./LoadingBar');
// Import the global data from the configuration file
const configGlobal = require('../client/config.global');

// Read the file with the owners
const fileJSON = fs.readFileSync('./proprietaire.json');
// Get the JSON object
const Tpro = JSON.parse(fileJSON).root.dataroot.T_proprietaire;

// Read the file with the streets' names and their codes which link to the owners' street codes
const fileJSON2 = fs.readFileSync('./nomRue.json');
// Get the JSON object
const TnomRue = JSON.parse(fileJSON2).root.dataroot.T_nom_de_la_rue;

// Read the file with the land lots' cadastral data
const fileJSON3 = fs.readFileSync('./cadastreParcelles.json');
// Get the JSON object
const cadastre = JSON.parse(fileJSON3).features;

// Connection to the MongoDB database
mongoose.connect('mongodb://localhost:27017/Mairie', { poolSize: 10, useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function (callback) {
    console.log('Base de données connectée');
});

// Create a new LoadingBar instance
const loadBar = new LoadingBar(Tpro.length, 'land lots');
// Land lot's number
var numeroPar = '';
// Street's id
var idRue = '';
// Street number
var numeroRue = '';
// Bis, ter, ...
let bisTer = '';
// Address supplement
let complement = '';
// Owner's id
var idPro = '';
// Owner's full name
let nomPrenom = [];
// Associated numbers
var numAssocies = [];
// Land lot's latitude
var latitude = '';
// Land lot's longitude
var longitude = '';

// Boolean to know if the object being added is valid
let isValid = true;
// Array to store the land lots' numbers that have already been added
var numPar = [];
// Array to store the land lots to add
var parToAdd = [];
// Array to store the cadastral data
var cadPar = [];
// Index to store the number of elements already added
let index = 0;

/**
 * Check if a value is correct
 * @param {The value to check} value 
 * @return {Boolean to know if the value is correct}
 */
function isCorrect(value) {
    return value !== null && value !== undefined && value.trim() !== '';
}

/**
 * Know if the geographical coordinate has to be corrected
 * @param {The value to correct} value 
 * @param {Boolean to indicate if the latitude has to be changed or the longitude} lat 
 * @return {Boolean to know if the coordinate has to be corrected}
 */
function correctCoordinates(value, lat) {
    const regLat = new RegExp(`/^${configGlobal.latitudePrefix}\.\d{6}$/g`);
    const regLon = new RegExp(`/^${configGlobal.longitudePrefix}\.\d{6}$/g`);
    if (lat) {
        return regLat.test(value.replace(/°/g, ''));
    } else {
        return regLon.test(value.replace(/°/g, ''));
    }
}

/**
 * Asynchronous and recursive function to search all the owners corresponding to the words given
 * @param {Array of words} words
 * @param {Index to look for} i
 * @return {The owners matching all the words}
 */
async function findData(words, i) {
    var results = await Proprietaire.find({
        $or: [
            {
                nom: {
                    $regex: words[i],
                    $options: 'i'
                }
            },
            {
                prenom: {
                    $regex: words[i],
                    $options: 'i'
                }
            },
            {
                nomEntreprise: {
                    $regex: words[i],
                    $options: 'i'
                }
            }
        ]
    });
    i++;
    if (i < words.length) {
        // Search the owners who have the next word in their name
        const resultat = await findData(words, i);
        return results.filter(function (n) {
            for (var i = 0; i < resultat.length; i++) {
                if (n.id === resultat[i].id) {
                    return true;
                }
            }
            return false;
        });
    } else {
        return results;
    }
}

// Delete all the land lots in the database
Parcelle.deleteMany({}, function (error) {
    if (error) {
        loadBar.displayMessage('ERROR : ' + error);
    } else {
        /**
         * Asynchronous function to add the land lots in the database
         */
        async function addPar() {
            // Start writing the first loading bar
            loadBar.init();

            // For each owner
            for (pro of Tpro) {
                // If the land lot has not already been added
                if (!numPar.includes(pro.N_parcelle.toUpperCase())) {
                    // Reset variables
                    nomPrenom = [];
                    numAssocies = [];
                    cadPar = [];
                    numeroPar = '';
                    idPro = '';
                    latitude = '';
                    longitude = '';
                    idRue = '';
                    numeroRue = '';
                    bisTer = '';
                    complement = '';
                    message = '';
                    isValid = true;

                    // Owner
                    if (pro.Nom_proprietaire !== undefined) {
                        nomPrenom = pro.Nom_proprietaire.split(' ');
                        if (nomPrenom.length > 0) {
                            // Call the recursive function to search the owner for the first word
                            const response = await findData(nomPrenom, 0);
                            if (response !== null && response !== undefined && response.length > 0) {
                                idPro = response[0]._id;
                            } else {
                                loadBar.displayMessage('ERROR : The owner has not been found');
                            }
                        } else {
                            isValid = false;
                        }
                    } else {
                        isValid = false;
                    }

                    // Geographical coordinates
                    if (isCorrect(pro.latitudePar) && isCorrect(pro.longitudePar) && correctCoordinates(pro.latitudePar, true) && correctCoordinates(pro.longitudePar, false)) {
                        latitude = pro.latitudePar.replace(/°/g, '');
                        longitude = pro.longitudePar.replace(/°/g, '');
                    } else {
                        isValid = false;
                    }

                    // Land lot's number
                    if (isCorrect(pro.N_parcelle)) {
                        numeroPar = pro.N_parcelle.toUpperCase();
                        let i = 0;
                        let found = false;
                        // Check if the land lot has a cadastre
                        while (i < cadastre.length && !found) {
                            // Search the cadastre associated to the land lot
                            let cad = cadastre[i].properties.section.concat(cadastre[i].properties.numero);
                            if (cad === numeroPar) {
                                found = true;
                                // Add the cadastral data into the array
                                cadastre[i].geometry.coordinates.forEach(element => {
                                    cadPar.push(element);
                                });
                            } else if (numeroPar.includes('/')) {
                                // Else, check for each part of the land lot's number if it exists
                                nPar = numeroPar.split('/');
                                nPar.forEach((element) => {
                                    if (cad === element) {
                                        found = true;
                                        // Add the cadastral data into the array
                                        cadastre[i].geometry.coordinates.forEach(element => {
                                            cadPar.push(element);
                                        });
                                    }
                                });
                            }
                            i++;
                        }
                    } else {
                        isValid = false;
                    }

                    // Street number
                    if (isCorrect(pro.Nouveau_numero)) {
                        numeroRue = pro.Nouveau_numero.trim();
                    } else {
                        isValid = false;
                    }

                    // BisTer
                    if (isCorrect(pro.Bis_ter)) {
                        bisTer = pro.Bis_ter.trim().toLowerCase();
                    }

                    // Find the street's name
                    if (isCorrect(pro.nom_rue)) {
                        var rue = TnomRue.find(rue => rue.nom_rue.toUpperCase() === pro.nom_rue.toUpperCase());
                        if (rue !== undefined) {
                            // Search in the database
                            const response = await Rue.findOne({ nom: rue.rue });
                            if (response !== null && response !== undefined) {
                                idRue = response._id;
                            } else {
                                isValid = false;
                                loadBar.displayMessage('ERROR : The street has not been found');
                            }
                        } else {
                            isValid = false;
                        }
                    } else {
                        isValid = false;
                    }

                    // Address supplement
                    if (isCorrect(pro.complement)) {
                        complement = pro.complement.trim();
                    }
                    
                    // Number associated
                    numAssocies.push({
                        numeroRue: numeroRue,
                        bisTer: bisTer,
                        complement: complement
                    });

                    if (isValid) {
                        // Create the new land lot
                        var new_parcelle = new Parcelle({});
                        new_parcelle.numero = numeroPar;
                        new_parcelle.proprietaire = idPro;
                        new_parcelle.latitude = latitude;
                        new_parcelle.longitude = longitude;
                        new_parcelle.numAssocies = numAssocies;
                        new_parcelle.rue = idRue;
                        new_parcelle.cadastre = cadPar;

                        // Add the land lot number in the array to store them
                        numPar.push(numeroPar);
                        // Add the land lot in the array to add them after
                        parToAdd.push(new_parcelle);
                    } else {
                        loadBar.displayMessage('ERROR : The land lot is not valid and has not been added');
                    }
                } else if (isCorrect(pro.complement)) {
                    // Else, see if a number associated can be added to a land lot that has already been added
                    // Find the land lot concerned
                    var parcelle = parToAdd.find(par => par.numero === pro.N_parcelle.toUpperCase());
                    if (parcelle !== undefined) {
                        // Reset variables
                        numeroRue = '';
                        bisTer = '';
                        complement = '';

                        // Street number
                        if (isCorrect(pro.Nouveau_numero)) {
                            numeroRue = pro.Nouveau_numero.trim();

                            // BisTer
                            if (isCorrect(pro.Bis_ter)) {
                                bisTer = pro.Bis_ter.trim().toLowerCase();
                            }

                            // Address supplement
                            if (isCorrect(pro.complement)) {
                                complement = pro.complement.trim();
                            }

                            // Number associated
                            parcelle.numAssocies.push({
                                numeroRue: numeroRue,
                                bisTer: bisTer,
                                complement: complement
                            });
                        }
                    }
                }
                index++;
                // Update the loading bar
                loadBar.updateBar(index);
            }

            console.log('Finalisation...');

            // Add all the land lots in the database
            for (par of parToAdd) {
                try {
                    await par.save();
                } catch (err) {
                    console.log('ERROR : ' + err);
                }
            }
        }
        addPar()
            .then(() => {
                // Disconnect from the database
                mongoose.connection.close();
                // Indicate the end of the script
                console.log('FIN INSERTION PARCELLES' + '\n');
            });
    }
});
