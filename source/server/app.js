/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/**
 * Main server-side's file.
 * 
 * Manage all the relations between the client-side and the server-side
 * and so between the client-side and the database.
 */
// Import modules
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const publicRoot = '../client/dist';
const history = require('connect-history-api-fallback');

// Create the Express app instance
const app = express();
// Set parameters and add the modules needed
app.use(history());
app.use(morgan('combined'));
// Tell the bodyparser middleware to accept more data
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(cors());
app.use(express.static(publicRoot));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(publicRoot));


// Import module to discuss with the database
var mongoose = require('mongoose');
// Connection to the MongoDB database
mongoose.connect('mongodb://localhost:27017/Mairie', { poolSize: 10, useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));
db.once('open', function (callback) {
    console.log('Base de données connectée');
});

// Import all the routers to discuss with the database elements
const rueDAO_Router = require('./routes/rueDAO');
const lieuditDAO_Router = require('./routes/lieuditDAO');
const parcelleDAO_Router = require('./routes/parcelleDAO');
const proprietaireDAO_Router = require('./routes/proprietaireDAO');
const propositionDAO_Router = require('./routes/propositionDAO');
const utilisateurDAO_Router = require('./routes/utilisateurDAO');
const roleDAO_Router = require('./routes/roleDAO');
const permissionDAO_Router = require('./routes/permissionDAO');
const connexion_Router = require('./routes/connexion');
const categorieMateriauDAO_Router = require('./routes/categorieMateriauDAO');
const phraseCommandeDAO_Router = require('./routes/phraseCommandeDAO');
const materiauDAO_Router = require('./routes/materiauDAO');
const articleDAO_Router = require('./routes/articleDAO');
const parametresDAO_Router = require('./routes/parametresDAO');
const courrierDAO_Router = require('./routes/courrierDAO');

// Add the routers' paths to the app instance
app.use('/rue', rueDAO_Router);
app.use('/lieuDit', lieuditDAO_Router);
app.use('/parcelle', parcelleDAO_Router);
app.use('/proprietaire', proprietaireDAO_Router);
app.use('/proposition', propositionDAO_Router);
app.use('/utilisateur', utilisateurDAO_Router);
app.use('/role', roleDAO_Router);
app.use('/permission', permissionDAO_Router);
app.use('/connexion', connexion_Router);
app.use('/categorieMateriau', categorieMateriauDAO_Router);
app.use('/materiau', materiauDAO_Router);
app.use('/phraseCommande', phraseCommandeDAO_Router);
app.use('/article', articleDAO_Router);
app.use('/parametres', parametresDAO_Router);
app.use('/courrier', courrierDAO_Router);

// Catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// Error handler
app.use(function (err, req, res, next) {
  // Set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // Render the error page
  res.sendStatus(err.status || 500);
});

// Export the app
module.exports = app;