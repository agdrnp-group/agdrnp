/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
/**
 * Manage all the streets, owners, land lots and street's name propositions in the database
 */
// Import the API to discuss with the database (server-side)
import Api from '@/services/Api';

// Export functions to discuss with the database
export default {
  /**
   * Find streets in the database
   * @param {Parameters for the query} params
   */
  findRue(params) {
    return Api().post('rue/rechercher', params);
  },
  /**
   * Find land lots in the database
   * @param {Parameters for the query} params
   */
  findPar(params) {
    return Api().post('parcelle/rechercher', params);
  },
  /**
   * Find all the streets attached to a said location in the database
   * @param {Parameters for the query} params
   */
  findLieu(params) {
    return Api().post('rue/rechercherLieu', params);
  },
  /**
   * Find owners in the database
   * @param {Parameters for the query} params
   */
  findPro(params) {
    return Api().post('proprietaire/rechercher', params);
  },
  /**
   * Get all the owners in the database
   * @param {Parameters for the query} params
   */
  findAllProprietaire(params) {
    return Api().post('proprietaire/rechercherTous', params);
  },
  /**
   * Find said locations in the database
   * @param {Parameters for the query} params
   */
  findLieuDit(params) {
    return Api().post('lieuDit/rechercher', params);
  },
  /**
   * Find all the land lots in a specific street
   * @param {Parameters for the query} params
   */
  findParcellesRue(params) {
    return Api().post('parcelle/rechercherRue', params);
  },
  /**
   * Find land lots corresponding to the number given
   * @param {Parameters for the query} params
   */
  findParcellesNumero(params) {
    return Api().post('parcelle/rechercherNumero', params);
  },
  /**
   * Find the land lot corresponding to a specific number
   * @param {Parameters for the query} params
   */
  findParNumero(params) {
    return Api().post('parcelle/rechercherPar', params);
  },

  /**
   * Add a street in the database
   * @param {Parameters for the query} params
   */
  add(params) {
    return Api().post('rue/ajouter', params);
  },
  /**
   * Add an owner in the database
   * @param {Parameters for the query} params
   */
  addPro(params) {
    return Api().post('proprietaire/ajouter', params);
  },
  /**
   * Add a land lot in the database
   * @param {Parameters for the query} params
   */
  addPar(params) {
    return Api().post('parcelle/ajouter', params);
  },

  /**
   * Update a specific street in the database
   * @param {Parameters for the query} params
   */
  updateRue(params) {
    return Api().put(`rue/modifier/${params.id}`, params);
  },
  /**
   * Get a specific street in the database
   * @param {Parameters for the query} params
   */
  getRue(params) {
    return Api().post(`rue/modifier/${params.id}`, params);
  },

  /**
   * Delete a specific street in the database
   * @param {Parameters for the query} params
   */
  deleteRue(params) {
    return Api().post(`rue/supprimer/${params.id}`, params);
  },

  /**
   * Get a specific land lot in the database
   * @param {Parameters for the query} params
   */
  getPar(params) {
    return Api().post(`parcelle/modifier/${params.id}`, params);
  },
  /**
   * Update a specific land lot in the database
   * @param {Parameters for the query} params
   */
  updatePar(params) {
    return Api().put(`parcelle/modifier/${params.id}`, params);
  },
  /**
   * Delete a specific land lot in the database
   * @param {Parameters for the query} params
   */
  deletePar(params) {
    return Api().post(`parcelle/supprimer/${params.id}`, params);
  },

  /**
   * Get a specific owner in the database
   * @param {Parameters for the query} params
   */
  getPro(params) {
    return Api().post(`proprietaire/modifier/${params.id}`, params);
  },
  /**
   * Update a specfic owner in the database
   * @param {Parameters for the query} params
   */
  updatePro(params) {
    return Api().put(`proprietaire/modifier/${params.id}`, params);
  },

  /**
   * Find street's name propositions in the databse
   * @param {Parameters for the query} params
   */
  findProposition(params) {
    return Api().post('proposition/rechercher', params);
  },
  /**
   * Add a street's name proposition in the database
   * @param {Parameters for the query} params
   */
  addProposition(params) {
    return Api().post('proposition/ajouter', params);
  },
  /**
   * Delete all the propositions of a specific street
   * @param {Parameters for the query} params
   */
  deleteAllProposition(params) {
    return Api().post(`proposition/supprimer/${params.nom}`, params);
  },
  /**
   * Delete a specific street's name proposition in the database
   * @param {Parameters for the query} params
   */
  deleteProposition(params) {
    return Api().post(`proposition/supprimerProp/${params.id}`, params);
  },
  /**
   * Get a specific street's name proposition
   * @param {Parameters for the query} params
   */
  getProposition(params) {
    return Api().post(`proposition/modifier/${params.id}`, params);
  },
  /**
   * Update a specific street's name proposition
   * @param {Parameters for the query} params
   */
  updateProposition(params) {
    return Api().put(`proposition/modifier/${params.id}`, params);
  },
};
