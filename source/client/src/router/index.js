/*
Copyright ou © ou Copr. Sébastien GAVIGNET et Alizée HAMON, (07/07/2020)

Contributeurs :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

Ce logiciel est un programme informatique servant à gérer l'urbanisme et la voirie
d'une collectivité à travers plusieurs outils :
   - Gestion des rues
   - Gestion des propriétaires
   - Gestion des parcelles
   - Gestion des lieux-dits
   - Commande de panneaux ou autres pour la signalisation
   - Gestion des propositions de noms de rues
   - Gestion de courriers pour du publipostage à destination des propriétaires. 

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et
respectant les principes de diffusion des logiciels libres. Vous pouvez
utiliser, modifier et/ou redistribuer ce programme sous les conditions
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA 
sur le site "http://www.cecill.info".

En contrepartie de l'accessibilité au code source et des droits de copie,
de modification et de redistribution accordés par cette licence, il n'est
offert aux utilisateurs qu'une garantie limitée. Pour les mêmes raisons,
seule une responsabilité restreinte pèse sur l'auteur du programme, le
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard l'attention de l'utilisateur est attirée sur les risques
associés au chargement, à l'utilisation, à la modification et/ou au
développement et à la reproduction du logiciel par l'utilisateur étant 
donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
manipuler et qui le réserve donc à des développeurs et des professionnels
avertis possédant des connaissances informatiques approfondies. Les
utilisateurs sont donc invités à charger et tester l'adéquation du
logiciel à leurs besoins dans des conditions permettant d'assurer la
sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les
termes.

-------------------------------------------------------------------------

Copyright or © or Copr. Sébastien GAVIGNET and Alizée HAMON (07/07/2020)

Contributors :
- Sébastien GAVIGNET (sebastien.gavignet22@orange.fr)
- Alizée HAMON (alize2301@gmail.com)
- Alex MAINGUY
- Aurélien MARCHAND
- Alan VADELEAU

This software is a computer program whose purpose is to manage the urban
planning and the roads of a community through several tools :
   - Street management
   - Owner management
   - Land lot management
   - Said location management
   - Orders of road signs and others
   - Management of street name suggestions
   - Mail management for direct mail to owners.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software. You can use, 
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors have only limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading, using, modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean that it is complicated to manipulate, and that also
therefore means that it is reserved for developers and experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and, more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/
// Import modules needed to create the Vue instance
import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuetify from 'vuetify';
import VueSession from 'vue-session';
import 'vuetify/dist/vuetify.min.css';

// Import the views for the application
import bddUser from '@/services/BddUser';
import materiaux from '../views/materiaux.vue';
import modifierCategorie from '../views/modifierCat.vue';
import supprimerCategorie from '../views/supprimerCat.vue';
import parametresMateriaux from '../views/parametresMateriaux.vue';
import modifierProposition from '../views/modifierProposition.vue';
import supprimerProposition from '../views/supprimerProposition.vue';
import ajouterArticle from '../views/ajouterArticle.vue';
import ajouterMateriau from '../views/ajouterMateriau.vue';
import telechargerDocument from '../views/telechargerDocument.vue';
import ajout from '../views/ajouterRue.vue';
import documents from '../views/documents.vue';
import editionCourrier from '../views/editCourrier.vue';
import telechargerCourrier from '../views/telechargerCourrier.vue';
import supprimerCourrier from '../views/supprimerCour.vue';
import compte from '../views/compte.vue';
import mesInformations from '../views/informations.vue';
import ajoutUtilisateur from '../views/ajouterUtilisateur.vue';
import modifierUtilisateur from '../views/modifierUtilisateur.vue';
import supprimerUtilisateur from '../views/supprimerUtilisateur.vue';
import gestionUtilisateurs from '../views/gestionUtilisateurs.vue';
import pageNotFound from '../views/pageNotFound.vue';
import modification from '../views/modifierRue.vue';
import suppression from '../views/supprimerRue.vue';
import modificationParcelle from '../views/modifierParcelle.vue';
import modificationProprietaire from '../views/modifierProprietaire.vue';
import suppressionParcelle from '../views/supprimerParcelle.vue';
import propositionRue from '../views/propositionRue.vue';
import choixProposition from '../views/choixProposition.vue';
import accueil from '../views/accueil.vue';
import modifierMateriau from '../views/modifierMat.vue';
import supprimerMateriau from '../views/supprimerMat.vue';
import modifierArticle from '../views/modifierArt.vue';
import supprimerArticle from '../views/supprimerArt.vue';

Vue.use(Vuetify);
Vue.use(VueRouter);
Vue.use(VueSession);

// Set which routes lead to which views
const routes = [
  {
    path: '/',
    name: 'Accueil',
    component: accueil,
  },
  {
    path: '/gestionUtilisateurs',
    name: 'GestionUtilisateurs',
    component: gestionUtilisateurs,
  },
  {
    path: '/mesInformations',
    name: 'MesInformations',
    component: mesInformations,
  },
  {
    path: '/compte',
    name: 'Compte',
    component: compte,
  },
  {
    path: '/documents',
    name: 'Documents',
    component: documents,
  },
  {
    path: '/editionCourrier',
    name: 'EditionCourrier',
    component: editionCourrier,
  },
  {
    path: '/telechargerCourrier',
    name: 'TelechargerCourrier',
    component: telechargerCourrier,
  },
  {
    path: '/supprimerCourrier',
    name: 'SupprimerCourrier',
    component: supprimerCourrier,
  },
  {
    path: '/telechargerDocument',
    name: 'TelechargerDocument',
    component: telechargerDocument,
  },
  {
    path: '/materiaux',
    name: 'Materiaux',
    component: materiaux,
  },
  {
    path: '/modifierCategorie',
    name: 'ModifierCategorie',
    component: modifierCategorie,
  },
  {
    path: '/supprimerCategorie',
    name: 'SupprimerCategorie',
    component: supprimerCategorie,
  },
  {
    path: '/parametresMateriaux',
    name: 'ParametresMateriaux',
    component: parametresMateriaux,
  },
  {
    path: '/ajouterMateriau',
    name: 'AjouterMateriau',
    component: ajouterMateriau,
  },
  {
    path: '/ajouterArticle',
    name: 'AjouterArticle',
    component: ajouterArticle,
  },
  {
    path: '/ajouter',
    name: 'Ajouter',
    component: ajout,
  },
  {
    path: '/ajouterUtilisateur',
    name: 'AjouterUtilisateur',
    component: ajoutUtilisateur,
  },
  {
    path: '/modifier',
    name: 'Modifier',
    component: modification,
  },
  {
    path: '/modifierUtilisateur',
    name: 'ModifierUtilisateur',
    component: modifierUtilisateur,
  },
  {
    path: '/supprimerUtilisateur',
    name: 'SupprimerUtilisateur',
    component: supprimerUtilisateur,
  },
  {
    path: '/supprimer',
    name: 'Supprimer',
    component: suppression,
  },
  {
    path: '/modifierParcelle',
    name: 'ModifierParcelle',
    component: modificationParcelle,
  },
  {
    path: '/modifierProprietaire',
    name: 'ModifierProprietaire',
    component: modificationProprietaire,
  },
  {
    path: '/supprimerParcelle',
    name: 'SupprimerParcelle',
    component: suppressionParcelle,
  },
  {
    path: '/proposition',
    name: 'Proposition',
    component: propositionRue,
  },
  {
    path: '/modifierProposition',
    name: 'modifierProposition',
    component: modifierProposition,
  },
  {
    path: '/supprimerProposition',
    name: 'supprimerProposition',
    component: supprimerProposition,
  },
  {
    path: '/choix',
    name: 'ChoixProposition',
    component: choixProposition,
  },
  {
    path: '/modifierMateriau',
    name: 'ModifierMateriau',
    component: modifierMateriau,
  },
  {
    path: '/supprimerMateriau',
    name: 'SupprimerMateriau',
    component: supprimerMateriau,
  },
  {
    path: '/modifierArticle',
    name: 'ModifierArticle',
    component: modifierArticle,
  },
  {
    path: '/supprimerArticle',
    name: 'SupprimerArticle',
    component: supprimerArticle,
  },
  {
    path: '*',
    name: 'PageNotFound',
    component: pageNotFound,
  },
];

// Create the router instance
const router = new VueRouter({
  mode: 'history',
  routes,
  // Go to the top of the new page
  scrollBehavior() {
    window.scrollTo(0, 0, 'smooth');
  },
});

/**
 * Check before each route is changed if the user has enough rights to continue
 */
router.beforeEach(async (to, from, next) => {
  // If the user is not logged in, return to the home page and the login page will load
  if (!router.app.$session.exists()) {
    if (to.path !== '/' || from.path !== '/') {
      return next('/');
    }
  } else {
    // Determine the actual user's role
    const roleUtil = await bddUser.findRole({ nomRole: router.app.$session.get('role'), token: router.app.$session.get('token') });
    // Get the list of permissions according to the role
    const droits = roleUtil.data[0].listePermission;
    // Determine if the actual user can access to the database management
    const verifGestInfo = await droits.filter(element => element.nomPermission === 'gestion des rues/par/pro');
    // Determine if the actual user can access to the user management
    const verifGestUtil = await droits.filter(element => element.nomPermission === 'gestion des utilisateurs');
    // Change the initial route if the user doesn't have enough rights to continue
    // Items management (streets, owners, land lots, ...)
    if (verifGestInfo.length === 0) {
      if (to.path === '/ajouter') {
        return next('/');
      }
      if (to.path === '/materiaux') {
        return next('/');
      }
    }
    // User management
    if (verifGestUtil.length === 0) {
      if (to.path === '/gestionUtilisateurs') {
        return next('/compte');
      }
    }
  }
  return next();
});

export default router;
